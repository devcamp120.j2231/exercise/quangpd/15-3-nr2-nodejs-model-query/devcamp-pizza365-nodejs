//middle ware lấy tất cả danh sách voucher
const getAllVoucherMiddleWare = (req,res,next)=>{
    console.log("Get All Voucher MiddleWare");
    next();
}

//middle ware lấy voucher by id
const getVoucherByIdMiddleWare = (req,res,next) =>{
    console.log("Get Voucher By Id MiddleWare");
    next();
}

//middle ware tạo một voucher mới
const postNewVoucherMiddleWare = (req,res,next) =>{
    console.log("Create New Voucher MiddleWare");
    next();
}

//middle ware sửa voucher by id
const putVoucherByIdMiddleWare = (req,res,next) =>{
    console.log("Update Voucher By Id MiddleWare");
    next();
}

//Middle ware xóa Voucher By id 
const deleteVoucherByIdMiddleWare = (req,res,next) =>{
    console.log("Delete Voucher By Id MiddleWare");
    next();
}

module.exports = {getAllVoucherMiddleWare , getVoucherByIdMiddleWare , postNewVoucherMiddleWare , putVoucherByIdMiddleWare , deleteVoucherByIdMiddleWare}
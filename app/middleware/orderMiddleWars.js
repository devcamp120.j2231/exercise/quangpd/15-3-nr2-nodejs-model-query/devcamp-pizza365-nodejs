//middle ware lấy tất cả danh sách Order
const getAllOrderMiddleWare = (req,res,next)=>{
    console.log("Get All Order MiddleWare");
    next();
}

//middle ware lấy Order by id
const getOrderByIdMiddleWare = (req,res,next) =>{
    console.log("Get Order By Id MiddleWare");
    next();
}

//middle ware tạo một Order mới
const postNewOrderMiddleWare = (req,res,next) =>{
    console.log("Create New Order MiddleWare");
    next();
}

//middle ware sửa Order by id
const putOrderByIdMiddleWare = (req,res,next) =>{
    console.log("Update Order By Id MiddleWare");
    next();
}

//Middle ware xóa Order By id 
const deleteOrderByIdMiddleWare = (req,res,next) =>{
    console.log("Delete Order By Id MiddleWare");
    next();
}

//
module.exports = {getAllOrderMiddleWare, getOrderByIdMiddleWare, postNewOrderMiddleWare , putOrderByIdMiddleWare, deleteOrderByIdMiddleWare}
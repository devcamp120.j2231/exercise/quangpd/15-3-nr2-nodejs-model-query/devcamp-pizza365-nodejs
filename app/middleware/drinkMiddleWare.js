//MiddleWare lấy tất cả danh sách đồ uống
const getAllDrinkMiddleWare = (req,res,next) =>{
    console.log("Get All Drink Middle Ware");
    next();
}
//  test
//MiddleWare tạo một đồ uống mới
const postNewDrinkMiddleWare = (req,res,next) =>{
    console.log("Create New Drink MiddleWares");
    next();
}

//MiddleWare lấy đồ uống by id
const getDrinkByIdMiddleWare = (req,res,next) =>{
    console.log("Get a Drink by Id MiddleWare");
    next();
}

//MiddleWare Sửa đồ uống 
const puttDrinkByIdMiddleWare = (req,res,next) =>{
    console.log("Update Drink By Id MiddleWare");
    next();
}

//MiddleWare Xóa đồ uống
const deleteDrinkByIdMiddleWare = (req,res,next) =>{
    console.log("Delete a Drink By Id MiddleWare");
    next();
}

module.exports = {getAllDrinkMiddleWare , postNewDrinkMiddleWare , getDrinkByIdMiddleWare , puttDrinkByIdMiddleWare , deleteDrinkByIdMiddleWare};
//middle ware lấy tất cả danh sách User
const getAllUserMiddleWare = (req,res,next)=>{
    console.log("Get All User MiddleWare");
    next();
}

//middle ware lấy User by id
const getUserByIdMiddleWare = (req,res,next) =>{
    console.log("Get User By Id MiddleWare");
    next();
}

//middle ware tạo một User mới
const postNewUserMiddleWare = (req,res,next) =>{
    console.log("Create New User MiddleWare");
    next();
}

//middle ware sửa User by id
const putUserByIdMiddleWare = (req,res,next) =>{
    console.log("Update User By Id MiddleWare");
    next();
}

//Middle ware xóa User By id 
const deleteUserByIdMiddleWare = (req,res,next) =>{
    console.log("Delete User By Id MiddleWare");
    next();
}

module.exports = {getAllUserMiddleWare , getUserByIdMiddleWare , postNewUserMiddleWare , putUserByIdMiddleWare , deleteUserByIdMiddleWare}
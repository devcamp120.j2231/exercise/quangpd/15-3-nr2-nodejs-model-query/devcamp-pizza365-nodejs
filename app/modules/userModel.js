//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng schema bao gồm các thuộc tính của collection trong mongo
const userSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    fullName :{
        type : String,
        required : true
    },
    email :{
        type : String,
        required : true,
        unique : true
    },
    address : {
        type : String,
        required : true
    },
    phone : {
        type : String,
        required : true,
        unique : true
    },
    orders : [
        {
            type : mongoose.Types.ObjectId,
            ref : 'order'
        }
    ]
},{
    timestamps : true
})

//Export Schema ra model
module.exports = mongoose.model("user",userSchema);
//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng schema bao gồm các thuộc tính của collection trong mongo
const drinkSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    maNuocUong :{
        type : String,
        unique : true,
        required : true
    },
    tenNuocUong :{
        type : String,
        required : true
    },
    donGia : {
        type : Number,
        required : true
    }
},{
    timestamps : true
})

//Export Schema ra model
module.exports = mongoose.model("drink",drinkSchema);
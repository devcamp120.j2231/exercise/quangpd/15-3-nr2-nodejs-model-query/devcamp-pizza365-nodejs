//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//
const randtoken = require('rand-token');

//Tạo đối tượng schema bao gồm các thuộc tính của collection trong mongo
const orderSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    orderCode :{
        type : String,
        default : function(){
            return randtoken.generate(64);
        },
        unique : true
    },
    pizzaSize :{
        type : String,
        required : true
    },
    pizzaType : {
        type : String,
        required : true
    },
    voucher : [
        {
            type : mongoose.Types.ObjectId,
            ref : 'voucher'
        }
    ],
    drink : [
        {
        type : mongoose.Types.ObjectId,
        ref : 'drink'
        }
    ],
    status : {
        type : String,
        required : true
    }
},{
    timestamps : true
})

//Export Schema ra model
module.exports = mongoose.model("order",orderSchema);
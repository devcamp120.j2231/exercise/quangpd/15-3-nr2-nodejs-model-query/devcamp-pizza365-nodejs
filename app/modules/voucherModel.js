//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng schema bao gồm các thuộc tính của collection trong mongo
const voucherSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    maVoucher :{
        type : String,
        unique : true,
        required : true
    },
    phanTramGiamGia :{
        type : Number,
        required : true
    },
    ghiChu : {
        type : String,
        required : false
    }
},{
    timestamps : true
})

//Export Schema ra model
module.exports = mongoose.model("voucher",voucherSchema);
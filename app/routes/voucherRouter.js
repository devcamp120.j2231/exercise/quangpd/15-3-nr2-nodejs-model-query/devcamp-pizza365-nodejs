//Import thư viện express
const express = require("express");

//Import controller
const { getAllVoucher, getVoucherById, createVoucher, updateVoucher, deleteVoucher } = require("../controllers/voucherController");

//Import Middleware
const { getAllVoucherMiddleWare, getVoucherByIdMiddleWare, postNewVoucherMiddleWare, putVoucherByIdMiddleWare, deleteVoucherByIdMiddleWare } = require("../middleware/voucherMiddleWare");

// Khai báo router
const voucherRouter = express.Router();

//middle ware dùng chung cho cả router
const voucherMiddleWare = (req,res,next) =>{
    console.log("MiddleWare For All VoucherRouter , Time : " + new Date() + "Method : " + req.method);
    next();
}

voucherRouter.use(voucherMiddleWare);


//-----------Phần Router------------ //
//Get All Voucher
voucherRouter.get("/voucher",getAllVoucherMiddleWare ,getAllVoucher);

//Get Voucher By Id 
voucherRouter.get("/voucher/:voucherId",getVoucherByIdMiddleWare,getVoucherById);

//Create New Voucher
voucherRouter.post("/voucher",postNewVoucherMiddleWare,createVoucher);

voucherRouter.put("/voucher/:voucherId",putVoucherByIdMiddleWare,updateVoucher);

voucherRouter.delete("/voucher/:voucherId",deleteVoucherByIdMiddleWare,deleteVoucher);

module.exports = { voucherRouter}
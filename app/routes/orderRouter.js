//Import thư viện express
const express = require("express");
//import controller
const { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById } = require("../controllers/orderController");

//Import MiddleWare
const { getAllOrderMiddleWare, getOrderByIdMiddleWare, postNewOrderMiddleWare, putOrderByIdMiddleWare, deleteOrderByIdMiddleWare } = require("../middleware/orderMiddleWars");

// Khai báo router
const orderRouter = express.Router();

//middle ware dùng chung cho cả router
const orderMiddleWare = (req,res,next) =>{
    console.log("MiddleWare For All orderRouter , Time : " + new Date() + "Method : " + req.method);
    next();
}

orderRouter.use(orderMiddleWare);

//-----------Phần Router------------ //
//Get all Order
orderRouter.get("/orders",getAllOrderMiddleWare,getAllOrder);

//Get order by orderId
orderRouter.get("/orders/:orderId",getOrderByIdMiddleWare,getOrderById);

//Create Order
orderRouter.post("/users/:userId/orders/:drinkId/:voucherId",postNewOrderMiddleWare,createOrder);

//Update Order By Id 
orderRouter.put("/orders/:orderId",putOrderByIdMiddleWare,updateOrderById);

//Delete Order By Id
orderRouter.delete("/orders/:orderId",deleteOrderByIdMiddleWare,deleteOrderById);

module.exports = { orderRouter}
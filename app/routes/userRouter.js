//Import thư viện express
const express = require("express");

//Import controller
const { getAllUser, createUser, getUserById, updateUserById, deleteUserByid, getAllUserLimit, getAllUserSkip, getAllUserSort, getAllUserSkipLimit, getAllUserSortSkipLimit } = require("../controllers/userController");

//Import middleware
const { getAllUserMiddleWare, getUserByIdMiddleWare, postNewUserMiddleWare, putUserByIdMiddleWare, deleteUserByIdMiddleWare } = require("../middleware/userMiddleWare");

// Khai báo router
const userRouter = express.Router();

//middle ware dùng chung cho cả router
const userMiddleWare = (req,res,next) =>{
    console.log("MiddleWare For All userRouter , Time : " + new Date() + "Method : " + req.method);
    next();
}

userRouter.use(userMiddleWare);

//-----------Phần Router------------ //

//Get All User Limit 
userRouter.get("/limit-users",getAllUserLimit);

//Get All User Skip
userRouter.get("/skip-users",getAllUserSkip);

//Get All User Sorted
userRouter.get("/sort-users",getAllUserSort);

//Get All User Skip Limit 
userRouter.get("/skip-limit-users",getAllUserSkipLimit);

//Get All User sort - skip - limit
userRouter.get("/sort-skip-limit-users",getAllUserSortSkipLimit);

//Get all User
userRouter.get("/users",getAllUserMiddleWare,getAllUser);

//Get user by userid
userRouter.get("/users/:userId",getUserByIdMiddleWare,getUserById);

//Create New User
userRouter.post("/users",postNewUserMiddleWare,createUser);

//Update user by id 
userRouter.put("/users/:userId",putUserByIdMiddleWare,updateUserById);

userRouter.delete("/users/:userId",deleteUserByIdMiddleWare,deleteUserByid);

module.exports = { userRouter}
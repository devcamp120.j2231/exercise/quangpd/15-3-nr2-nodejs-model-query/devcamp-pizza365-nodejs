//khai báo thư viện express
const express = require("express");
//Import controller
const { getAllDrink, getDrinkById, createDrink, updateDrink, deleteDrink } = require("../controllers/drinkController");

//Import MiddleWare
const { getAllDrinkMiddleWare, getDrinkByIdMiddleWare, postNewDrinkMiddleWare, puttDrinkByIdMiddleWare, deleteDrinkByIdMiddleWare } = require("../middleware/drinkMiddleWare");

//Khai báo router 
const drinkRouter = express.Router();

const drinkMiddleWare = (req,res,next) =>{
    console.log("Drink MiddleWare - Time :" + new Date() + "- Method : " + req.method);

    next();
}
drinkRouter.use(drinkMiddleWare);

//----------------- Phần router----------------//
//Get all Drink
drinkRouter.get("/drink",getAllDrinkMiddleWare,getAllDrink);

//Get Drink By Id
drinkRouter.get("/drink/:maDoUong",getDrinkByIdMiddleWare,getDrinkById);

//Create new drink
drinkRouter.post("/drink",postNewDrinkMiddleWare,createDrink);

//Update drink by id 
drinkRouter.put("/drink/:maDoUong",puttDrinkByIdMiddleWare ,updateDrink);

//Delete Drink by id 
drinkRouter.delete("/drink/:maDoUong",deleteDrinkByIdMiddleWare,deleteDrink);

//Export module
module.exports = {drinkRouter}






//Middleware chung cho cả router
drinkRouter.use(drinkMiddleWare);




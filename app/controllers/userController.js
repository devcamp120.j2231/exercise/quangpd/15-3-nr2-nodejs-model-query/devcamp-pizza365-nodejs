//Import thư viện mongoose
const mongoose = require("mongoose");

//Import usermodel 
const userModel = require("../modules/userModel");

//get all user limit
const getAllUserLimit = (req,res) =>{
    //B1: thu thập dữ liệu
    let limitUser = req.query.limitUser;


    //B2 : Kiểm tra dữ liệu

    //B3 : Xử lý và trả về 
    userModel.find().limit(limitUser).exec((err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get User , Limit = ${limitUser}`,
                user : data
            })
        }
    });

}

//Get All User Skip
const getAllUserSkip = (req,res) =>{
    //B1 : thu thập dữ liệu
    let skipUser = req.query.skipUser;

    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    userModel.find().skip(skipUser).exec((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server err : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message : `Load All user Skip : ${skipUser}`,
                users : data
            })
        }
    })

}

//get All User Sort
const getAllUserSort = (req,res) =>{
    //B1 : thu thập dữ liệu
    let sortUser = req.query.sortUser;
    //Sort có 2 kiểu :  asc|desc
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    userModel.find().sort({fullName : sortUser}).exec((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Load All User By Sort : ${sortUser}`,
                user : data
            })
        }
    })

}

//Get All User Skip Limit
const getAllUserSkipLimit = (req,res) =>{
    //B1 : thu thập dữ liệu
    let skipUser = req.query.skipUser;
    let limitUser = req.query.limitUser;

    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về 
    userModel.find().skip(skipUser).limit(limitUser).exec((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Load User By Skip = ${skipUser} , Limit = ${limitUser}`,
                users : data
            })
        }
    })
}

//Get All User Sort Skip Limit 
const getAllUserSortSkipLimit = (req,res) =>{
    //B1 : thu thập dữ liệu
    let sortUser = req.query.sortUser;
    let skipUser = req.query.skipUser;
    let limitUser = req.query.limitUser;

    //B2 : kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    userModel.find().sort({fullName : sortUser}).skip(skipUser).limit(limitUser).exec((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message : `Load All User Sort By : ${sortUser} Skip By : ${skipUser} and Limit By : ${limitUser}`,
                user : data
            })
        }
    })
}

//get all user
const getAllUser = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : kiểm tra dữ liệu
    //B3 : Xử lý và trả về 
    console.log("Get All User");
    userModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Get All User Successfully`,
                users : data
            })
        }
    })

}

//create user
const createUser = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;
    //B2 : kiểm tra dữ liệu
    if(!body.fullName){
        return res.status(400).json({
            message : `Bad Request - fullName is required`
        })
    }

    if(!body.email){
        return res.status(400).json({
            message : `Bad Request - email is required`
        })
    }

    if(!body.address){
        return res.status(400).json({
            message : `Bad Request - address is required`
        })
    }

    if(!body.phone){
        return res.status(400).json({
            message : `Bad Request - phone is required`
        })
    }

    //B3 : xử lý và trả về dữ liệu
    console.log("Create New User !");
    let newUser = new userModel({
        _id : mongoose.Types.ObjectId(),
        fullName : body.fullName,
        email : body.email,
        address : body.address,
        phone : body.phone
    });

    userModel.create(newUser , (err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Create User Successfully `,
                users : data
            })
        }
    })


}

//get user by userid
const getUserById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message : `Bad Request - Id is invalid`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    userModel.findById(userId ,(err,data) =>{
        if(err){
           return res.status(500).json({
                message : `Internal server error : ${err.message}`
           })
        }else{
            return res.status(201).json({
                message : `Get User By Id successfully`,
                user : data
            })
        }

    })
}

//update user by user id 
const updateUserById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;
    var body = req.body;

    //B2 : kiểm tra dữ liệu 
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message :`Bad Request - userId is not valid`
        })
    }

    if(!body.fullName){
        return res.status(400).json({
            message : `Bad Request - fullName is required`
        })
    }

    if(!body.email){
        return res.status(400).json({
            message : `Bad Request - email is required`
        })
    }

    if(!body.address){
        return res.status(400).json({
            message : `Bad Request - address is required`
        })
    }

    if(!body.phone){
        return res.status(400).json({
            message : `Bad Request - phone is required`
        })
    }

    //B3 : xử lý và trả về dữ liệu
    let updateUser = new userModel({
        fullName : body.fullName,
        email : body.email,
        address : body.address,
        phone : body.phone
    });

    userModel.findByIdAndUpdate(userId,updateUser,(err,data)=>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Update User By Id Successfully !`,
                user : data
            })
        }
    })
}

//Delete user by user id 
const deleteUserByid = (req,res) =>{
    //B1:Thu thập dữ liệu
    let userId = req.params.userId;
    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message: `Bad Request - userId is not valid`
        })
    }

    //B3 : Xử lý dữ liệu và trả về kết quả 
    userModel.findByIdAndDelete(userId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).json({
                message :`Delete User By Id Successfully !`
            })
        }
    })
}
//delete user by user id 
//Export thành module
module.exports = { getAllUser ,createUser ,getUserById ,updateUserById ,deleteUserByid, getAllUserLimit , getAllUserSkip , getAllUserSort, getAllUserSkipLimit , getAllUserSortSkipLimit};
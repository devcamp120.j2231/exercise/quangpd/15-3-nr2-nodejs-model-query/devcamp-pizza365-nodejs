//import thư viện mongoose
const mongoose = require("mongoose");

//Import user model và order model
const userModel = require("../modules/userModel");
const orderModel = require("../modules/orderModel");

//create order 
const createOrder = (req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;
    let voucherId = req.params.voucherId;
    let drinkId = req.params.drinkId;
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message : `Bad Request - userId is not valid !`
        })
    }

    if(!body.pizzaSize){
        return res.status(400).json({
            message : `Bad Request - pizzaSize is not valid !`
        })
    }

    if(!body.pizzaType){
        return res.status(400).json({
            message : `Bad Request - pizzaType is not valid !`
        })
    }
    
    if(!body.status){
        return res.status(400).json({
            message : `Bad Request - status is not valid !`
        })
    }

    //B3 : xử lý và trả về dữ liệu
    let newOrder = new orderModel({
        _id : mongoose.Types.ObjectId(),
        // orderCode : body.orderCode,
        pizzaSize : body.pizzaSize,
        pizzaType : body.pizzaType,
        voucher : voucherId,
        drink : drinkId,
        status : body.status
    });

    orderModel.create(newOrder ,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }

        //Khi tạo xong order cần thêm id order mới vào mảng order của user
        userModel.findByIdAndUpdate(userId,{
            $push : {orders : data._id}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error`
                })
            }else{
                return res.status(201).json({
                    message : `Create Order Successfully`,
                    data : data
                })
            }
        })
    })
}

//get all Order
const getAllOrder = (req,res) =>{
    //B1: thu thập dữ liệu
    //B2 : kiểm tra dữ liệu
    //B3 : xử lý và trả về dữ liệu
    orderModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Load all Order Succesfully`,
                orders : data
            })
        }
    })
}

//get Order by OrderId
const getOrderById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad request - OrderId is not valid`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    orderModel.findById(orderId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Get Order By Id Successfully !`,
                order : data
            })
        }
    })
}

//Update order by orderId
const updateOrderById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad request - Order Id is not valid`
        })
    }

    if(!mongoose.Types.ObjectId.isValid(body.voucher)){
        return res.status(400).json({
            message :`Bad request - Voucher Id is not valid`
        })
    }

    if(!mongoose.Types.ObjectId.isValid(body.drink)){
        return res.status(400).json({
            message :`Bad request - Drink Id is not valid`
        })
    }

    if(!body.pizzaSize){
        return res.status(400).json({
            message :`Bad request - pizzaSize is not valid`
        })
    }

    if(!body.pizzaType){
        return res.status(400).json({
            message :`Bad request - pizzaType is not valid`
        })
    }

    if(!body.status){
        return res.status(400).json({
            message :`Bad request - status is not valid`
        })
    }

    //B3 : xử lý và trả về kết quả 
    let updateOrder = new orderModel ({
        pizzaSize : body.pizzaSize,
        pizzaType : body.pizzaType,
        voucher : body.voucher,
        drink : body.drink,
        status : body.status
    })

    orderModel.findByIdAndUpdate(orderId,updateOrder ,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Update Order Successfully !`,
                order : data
            })
        }
    })


}

//Delete order by OrderId 
const deleteOrderById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;
    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad Request - Order Id is not valid`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    orderModel.findByIdAndDelete(orderId ,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).json({
                message : `Delete Order By Id Succesfully`,
                order : data
            })
        }
    })
}
//Export thành module
module.exports = {createOrder , getAllOrder , getOrderById , updateOrderById , deleteOrderById};
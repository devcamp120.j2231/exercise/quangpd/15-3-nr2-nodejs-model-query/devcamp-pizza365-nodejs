//Import voucher model
const { default: mongoose } = require("mongoose");
const voucherModel = require("../modules/voucherModel");

//get all voucher
const getAllVoucher = (req,res) =>{
    //B1 : thu thập dữ liệu

    //B2 : kiểm tra dữ liệu

    //B3 : Xử lý và trả về kết quả
    console.log("Get all Voucher");
    voucherModel.find((error,data)=>{
        if(error){
            return res.status(500).json({
                message : `Internal server error :${error.message}`
            })
        }else{
            return res.status(201).json({
                message : `Successfully load all data voucher`,
                voucher : data
            })
        }
    })

    
}

//get a voucher
const getVoucherById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let voucherId = req.params.voucherId;
    
    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(400).json({
            message : `Id is invalid`
        })
    }

    //B3 : xử lý và trả về kết quả
    voucherModel.findById(voucherId,(error,data) =>{
        if(error){
            return res.status(500).json({
                message :`internal server error`
            })
        }else{
            return res.status(200).json({
                message :`Successfully load voucher by id`,
                voucher : data
            })
        }
    })
}

//Create new drink
const createVoucher = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!body.maVoucher){
        return res.status(400).json({
            message : `maVoucher is required`
        })
    }

    if(!body.phanTramGiamGia || Number.isInteger(body.phanTramGiamGia)){
        return res.status(400).json({
            message : `tenNuocUong is required`
        })
    }


    //B3 :Xử lý và trả về kết quả
    console.log("Create New Voucher");
    let newVoucher = new voucherModel({
        _id : mongoose.Types.ObjectId(),
        maVoucher : body.maVoucher,
        phanTramGiamGia : body.phanTramGiamGia,
        ghiChu : body.ghiChu
    })

    voucherModel.create(newVoucher ,(error,data) =>{
        if(error){
            return res.status(500).json({
                message : ` Internal server error`
            })
        } else{
            return res.status(201).json({
                message :`Successfully Created Voucher`,
                voucher : data
            })
        }
    })
    
    
    
}

//Update a drink
const updateVoucher = (req,res) =>{
    //B1 : thu thập dữ liệu
    let id = req.params.voucherId;
    let body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message :`Id is invalid`
        })
    }

    if(!body.maVoucher){
        return res.status(400).json({
            message : `maVoucher is required`
        })
    }

    if(!body.phanTramGiamGia || Number.isInteger(body.phanTramGiamGia)){
        return res.status(400).json({
            message : `tenNuocUong is required`
        })
    }
   

    //B3 : Xử lý và trả về kết quả
    console.log("Update Voucher");
    let newVoucher = new voucherModel({
        maVoucher : body.maVoucher,
        phanTramGiamGia : body.phanTramGiamGia,
        ghiChu : body.ghiChu
    });

    voucherModel.findByIdAndUpdate(id,newVoucher,(error,data)=>{
        if(error){
            return res.status(500).json({
                message : `Internal server error : ${error.message}`
            })
        }else {
            return res.status(201).json({
                message : `Update Successfully Voucher`,
                drink : data
            })
        }
    })
}

//Delete drink 
const deleteVoucher = (req,res) =>{
    //B1 : thu thập dữ liệu
    let id = req.params.voucherId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message :` Id is invalid`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    console.log("Delete a Voucher" + id);
    voucherModel.findByIdAndDelete(id,(error,data) =>{
        if(error){
            return res.status(500).json({
                message : "Internal server error"
            })
        }else  {
            return res.status(204).json({
                message : "Delete voucher Successfully",
                voucher : data
            })
        }
    })

}

// Export thành module
module.exports = {getAllVoucher , getVoucherById , createVoucher , updateVoucher, deleteVoucher};
//import drink model
const { default: mongoose } = require("mongoose");
const drinkModel = require("../modules/drinkModel");

//get all drink 
const getAllDrink = (req,res) =>{
    //B1 : thu thập dữ liệu

    //B2 : kiểm tra dữ liệu

    //B3 : Xử lý và trả về kết quả
    console.log("Get all Drink");
    drinkModel.find((error,data)=>{
        if(error){
            return res.status(500).json({
                message : `Internal server error :${error.message}`
            })
        }else{
            return res.status(201).json({
                message : `Successfully load all data drink`,
                drink : data
            })
        }
    })

    
}

//get a drink
const getDrinkById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let id = req.params.maDoUong;
    
    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message : `Id is invalid`
        })
    }

    //B3 : xử lý và trả về kết quả
    drinkModel.findById(id,(error,data) =>{
        if(error){
            return res.status(500).json({
                message :`internal server error`
            })
        }else{
            return res.status(200).json({
                message :`Successfully load drink by id`,
                drink : data
            })
        }
    })
}

//Create new drink
const createDrink = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!body.maNuocUong){
        return res.status(400).json({
            message : `maNuocUong is required`
        })
    }

    if(!body.tenNuocUong){
        return res.status(400).json({
            message : `tenNuocUong is required`
        })
    }

    if(Number.isInteger(body.donGia) || !body.donGia){
        return res.status(400).json({
            message : `donGia is required`
        })
    }

    //B3 :Xử lý và trả về kết quả
    console.log("Create New Drink");
    let newDrink = new drinkModel({
        _id : mongoose.Types.ObjectId() ,
        maNuocUong : body.maNuocUong,
        tenNuocUong : body.tenNuocUong,
        donGia : body.donGia
    })

    drinkModel.create(newDrink ,(error,data) =>{
        if(error){
            return res.status(500).json({
                message : ` Internal server error`
            })
        } else{
            return res.status(201).json({
                message :`Successfully Created Drink`,
                drink : data
            })
        }
    })
    
    
    
}

//Update a drink
const updateDrink = (req,res) =>{
    //B1 : thu thập dữ liệu
    let id = req.params.maDoUong;
    let body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message :`Id is invalid`
        })
    }

    if(!body.maNuocUong){
        return res.status(400).json({
            message :`maNuocUong is required`
        })
    }

    if(!body.tenNuocUong){
        return res.status(400).json({
            message :`tenNuocUong is required`
        })
    }

    if(Number.isInteger(body.donGia) || !body.donGia){
        return res.status(400).json({
            message :`donGia is required`
        })
    }

    //B3 : Xử lý và trả về kết quả
    console.log("Update New Drink");
    let newDrink = new drinkModel({
        maNuocUong : body.maDoUong,
        tenNuocUong : body.tenNuocUong,
        donGia : body.donGia
    });

    drinkModel.findByIdAndUpdate(id,newDrink,(error,data)=>{
        if(error){
            return res.status(500).json({
                message : `Internal server error : ${error.message}`
            })
        }else {
            return res.status(201).json({
                message : `Update Successfully`,
                drink : data
            })
        }
    })
}

//Delete drink 
const deleteDrink = (req,res) =>{
    //B1 : thu thập dữ liệu
    let id = req.params.maDoUong;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message :` Id is invalid`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    console.log("Delete a Drink" + id);
    drinkModel.findByIdAndDelete(id,(error,data) =>{
        if(error){
            return res.status(500).json({
                message : "Internal server error"
            })
        }else  {
            return res.status(204).json({
                message : "Delete Drink Successfully",
                drink : data
            })
        }
    })

}

//B3 : export thành module 
module.exports = { getAllDrink ,getDrinkById ,createDrink ,updateDrink ,deleteDrink }
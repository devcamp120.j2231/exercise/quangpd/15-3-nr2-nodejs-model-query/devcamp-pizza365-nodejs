//Import thư viện express
const express = require("express");

//Khai báo mongoose 
const mongoose = require("mongoose");

//Khai báo các model mongoose
const drinkModel = require("./app/modules/drinkModel");
const voucherModel = require("./app/modules/voucherModel");
const orderModel = require("./app/modules/orderModel");
const userModel = require("./app/modules/userModel");

//Khai báo các router
const { drinkRouter } = require("./app/routes/drinkRoutes");
const { orderRouter } = require("./app/routes/orderRouter");
const { userRouter } = require("./app/routes/userRouter");
const { voucherRouter } = require("./app/routes/voucherRouter");

// Tạo app 
const app = express();

//Khai một cổng 
const port = 8000;

//Khai báo sử dụng json
app.use(express.json());

//Kết nối CSDL (sử dụng mongoose theo phiên bản 6x)
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365",(error) =>{
    if(error) throw error;
    console.log("Connect Successfully ! ");
});

app.use('/',drinkRouter);
app.use('/',voucherRouter);
app.use('/',userRouter);
app.use('/',orderRouter);

//Khai báo app tại port 8000

app.listen(port, () =>{
    console.log(`App listening on port `,port);
}) 
